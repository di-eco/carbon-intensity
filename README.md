carbon-intensity
================

## Intro
Technical app to show data from https://api.carbonintensity.org.uk/regional/intensity/{from}/fw24h

## Notes
As the spec said to use /regional/intensity/{from}/fw24h that is what I have done and the data from there is how I have arranged the app.

I think that a better approach would have been to use /regional/intensity/{from}/fw24h/regionid/{regionid} and reverse the periods and regions in the UI.
This would result in the smaller and fixed dimension of regions as tabs with the longer dimension for periods, which would make for better UX.
Also I feel that most users would prefer to browse by region than by time period. 
Lastly this would also allow for reduced API calls, as there would only be 1 call per region that the user looks at rather than always retrieving all the data in one big hit.