package com.example.carbonintensity

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import org.apache.commons.io.IOUtils
import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Test
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit

class PeriodTest {

    val testKodein = Kodein {
        bind<CarbonIntensityAPI>() with singleton {
            val network = NetworkBehavior.create()
            network.setDelay(1, TimeUnit.MILLISECONDS)
            network.setErrorPercent(0)
            network.setVariancePercent(0)

            MockRetrofit.Builder(CarbonIntensityAPIModule.retrofit)
                    .networkBehavior(network)
                    .build()
                    .create(CarbonIntensityAPI::class.java)
                    .returningResponse(responseFromJson("fw24h_success.json"))
        }
    }

    private val api = testKodein.instance<CarbonIntensityAPI>()

    @Test
    fun testJSON() {
        val response = api.carbonIntensityFor24h(DateTime.now()).execute().body()
        Assert.assertNotNull(response)
        Assert.assertEquals(49, response!!.data.size)
        Assert.assertEquals(DateTime(2018, 5, 15, 12, 30), response.data.first().from)
    }

    private fun responseFromJson(file: String): CarbonIntensityResponse {
        val responseStream = ClassLoader.getSystemClassLoader().getResourceAsStream(file)
        val response = IOUtils.toString(responseStream)
        return CarbonIntensityAPIModule.gson.fromJson(response, CarbonIntensityResponse::class.java)
    }

}