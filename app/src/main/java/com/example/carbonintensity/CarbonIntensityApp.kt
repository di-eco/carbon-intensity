package com.example.carbonintensity

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.android.androidModule
import com.github.salomonbrys.kodein.lazy

class CarbonIntensityApp : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        import(androidModule)
        import(CarbonIntensityAPIModule().module)
    }

}

/**
 * ViewModel val property delegate for a model with it's own factory, passes the applications Kodein instance.
 * <code>private val viewModel: ViewModel by viewModelProvider { ViewModel(it) }</code>
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified VM : ViewModel> AppCompatActivity.viewModelProvider(
        crossinline provider: (Kodein) -> VM) = lazy {
    ViewModelProviders.of(this, object : ViewModelProvider.Factory {
        override fun <T1 : ViewModel> create(aClass: Class<T1>) =
                provider((application as CarbonIntensityApp).kodein) as T1
    }).get(VM::class.java)
}