package com.example.carbonintensity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.period_activity.*
import kotlinx.android.synthetic.main.period_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Gets a list of periods for the next 24 hours from the Carbon Intensity API and displays a
 * swipe-able tab list of PeriodFragments for each one returned.
 */
class PeriodActivity : AppCompatActivity() {

    companion object {
        private val SubtitleFormatter = DateTimeFormat.mediumDateTime()
    }

    private val viewModel: PeriodViewModel by viewModelProvider { PeriodViewModel(it) }
    private val periodFragmentPagerAdapter by lazy { PeriodFragmentPagerAdapter(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.period_activity)
        setSupportActionBar(toolbar)
        view_pager.adapter = periodFragmentPagerAdapter
        tab_layout.setupWithViewPager(view_pager)
        viewModel.date.observe(this, Observer {
            // Set the title to the date searched for
            toolbar.subtitle = getString(R.string.subtitle_format, it?.toString(SubtitleFormatter) ?: "")
        })
        viewModel.carbonIntensity.observe(this, Observer {
            // Update the list of dates shown
            periodFragmentPagerAdapter.data = it
            progress.visibility = View.GONE
        })
        viewModel.loadCarbonIntensity()
    }
}

/**
 * Adapter for a list of CarbonIntensityData to create PeriodFragments.
 */
class PeriodFragmentPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    companion object {
        private val TabFormatter = DateTimeFormat.shortTime()
    }

    var data: List<CarbonIntensityData>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getCount() = data?.size ?: 0

    override fun getItem(position: Int) = PeriodFragment.newInstance(data!![position])

    override fun getPageTitle(position: Int): String = data!![position].to.toString(TabFormatter)

}

/**
 * Shows a list of regions for a period with their intensity index.
 */
class PeriodFragment : Fragment() {

    companion object {
        private const val ExtraPeriod = "PeriodFragment.Period"

        fun newInstance(period: CarbonIntensityData): PeriodFragment {
            val frag = PeriodFragment()
            val args = Bundle()
            args.putParcelable(ExtraPeriod, period)
            frag.arguments = args
            return frag
        }
    }

    private val period by lazy { arguments!!.getParcelable<CarbonIntensityData>(ExtraPeriod)!! }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.period_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        region_list.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))
        region_list.adapter = RegionAdapter(period)
    }

}

/**
 * Adapter for the list of regions in a period.
 */
class RegionAdapter(private val period: CarbonIntensityData) : RecyclerView.Adapter<RegionViewHolder>() {

    private val regions = period.regions.sortedBy { it.shortName }

    override fun getItemCount() = regions.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RegionViewHolder(parent, period)

    override fun onBindViewHolder(holder: RegionViewHolder, position: Int) = holder.bind(regions[position])

}

/**
 * Shows a single region with intensity index info.
 */
class RegionViewHolder(parent: ViewGroup, private val period: CarbonIntensityData) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.region_item, parent, false)) {

    private val nameView: TextView by lazy { itemView.findViewById<TextView>(R.id.region_name)  }
    private var region: CarbonIntensityRegion? = null

    init {
        itemView.setOnClickListener { region?.let { itemView.context.startActivity(PeriodRegionActivity.createIntent(itemView.context, period, it)) } }
    }

    fun bind(region: CarbonIntensityRegion) {
        this.region = region
        nameView.text = itemView.context.getString(R.string.region_format, region.shortName, region.intensity.index, region.intensity.forecast)
    }

}


/**
 * Period view model to hold carbon intensity data.
 */
class PeriodViewModel(kodein: Kodein): ViewModel() {

    /* The date the current API results are searched from */
    val date = MutableLiveData<DateTime?>()
    /* The last API results */
    val carbonIntensity = MutableLiveData<List<CarbonIntensityData>?>()

    private val api: CarbonIntensityAPI = kodein.instance()

    fun loadCarbonIntensity() {
        loadCarbonIntensity(DateTime.now())
    }

    private fun loadCarbonIntensity(from: DateTime) {
        GlobalScope.launch {
            val response = api.carbonIntensityFor24h(from).execute()
            if (response.isSuccessful && response.body() != null) {
                date.postValue(from)
                carbonIntensity.postValue(response.body()!!.data)
            }
            else {
                TODO("Exception handling")
            }
        }
    }

}