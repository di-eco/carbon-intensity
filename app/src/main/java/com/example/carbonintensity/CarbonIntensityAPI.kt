package com.example.carbonintensity

import android.os.Parcelable
import com.fatboyindustrial.gsonjodatime.Converters
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

class CarbonIntensityAPIModule {

    companion object {
        val gson = Converters.registerDateTime(GsonBuilder()).create()
        val retrofit: Retrofit = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(CarbonIntensityAPI.baseURL)
                        .build()
    }

    val module = Kodein.Module {
        bind<CarbonIntensityAPI>() with singleton {
            retrofit.create(CarbonIntensityAPI::class.java)
        }
    }

}

interface CarbonIntensityAPI {

    companion object {
        const val baseURL = "https://api.carbonintensity.org.uk"
    }

    @GET("/regional/intensity/{from}/fw24h")
    fun carbonIntensityFor24h(
            @Path("from") from: DateTime
    ): Call<CarbonIntensityResponse>

}

data class CarbonIntensityResponse(
        @SerializedName("data") val data: List<CarbonIntensityData>
)

@Parcelize
data class CarbonIntensityData(
        @SerializedName("from") val from: DateTime,
        @SerializedName("to") val to: DateTime,
        @SerializedName("regions") val regions: List<CarbonIntensityRegion>
) : Parcelable

@Parcelize
data class CarbonIntensityRegion(
        @SerializedName("regionid") val regionId: Int,
        @SerializedName("dnoregion") val dnoRegion: String,
        @SerializedName("shortname") val shortName: String,
        @SerializedName("intensity") val intensity: CarbonIntensityIntensity,
        @SerializedName("generationmix") val generationMix: List<CarbonIntensityGenerationMix>
) : Parcelable

@Parcelize
data class CarbonIntensityIntensity(
        @SerializedName("forecast") val forecast: Int,
        @SerializedName("index") val index: String
) : Parcelable

@Parcelize
data class CarbonIntensityGenerationMix(
        @SerializedName("fuel") val fuel: String,
        @SerializedName("perc") val percentage: Float
) : Parcelable