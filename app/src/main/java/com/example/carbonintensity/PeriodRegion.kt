package com.example.carbonintensity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.period_region_activity.*
import org.joda.time.format.DateTimeFormat

/**
 * Shows the detailed information for a region at a period of time.
 */
class PeriodRegionActivity : AppCompatActivity() {

    companion object {
        private const val ExtraPeriod = "PeriodRegion.Period"
        private const val ExtraRegion = "PeriodRegion.Region"
        private val SubtitleFormatter = DateTimeFormat.shortTime()

        fun createIntent(ctx: Context, period: CarbonIntensityData, region: CarbonIntensityRegion): Intent {
            val intent = Intent(ctx, PeriodRegionActivity::class.java)
            intent.putExtra(ExtraPeriod, period)
            intent.putExtra(ExtraRegion, region)
            return intent
        }
    }

    private val period by lazy { intent.getParcelableExtra<CarbonIntensityData>(ExtraPeriod)!! }
    private val region by lazy { intent.getParcelableExtra<CarbonIntensityRegion>(ExtraRegion)!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.period_region_activity)
        setSupportActionBar(toolbar)
        title = region.shortName
        toolbar.subtitle = getString(R.string.region_subtitle_format, period.from.toString(SubtitleFormatter), period.to.toString(SubtitleFormatter))
        mix_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        mix_list.adapter = GenerationMixAdapter(region.generationMix.sortedByDescending { it.percentage })
    }

}

/**
 * Adapter for the list of generation mix in a region for a period.
 */
class GenerationMixAdapter(private val mix: List<CarbonIntensityGenerationMix>) : RecyclerView.Adapter<GenerationMixViewHolder>() {

    override fun getItemCount() = mix.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = GenerationMixViewHolder(parent)

    override fun onBindViewHolder(holder: GenerationMixViewHolder, position: Int) = holder.bind(mix[position])

}

/**
 * Shows a single generation mix row.
 */
class GenerationMixViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.generation_mix_item, parent, false)) {

    private val nameView: TextView by lazy { itemView.findViewById<TextView>(R.id.region_name)  }

    fun bind(generationMix: CarbonIntensityGenerationMix) {
        nameView.text = itemView.context.getString(R.string.generation_mix_format, generationMix.percentage, generationMix.fuel)
    }

}